angular.module('todo', ['ionic',"highcharts-ng"])
/**
 * The Projects factory handles saving and loading projects
 * from local storage, and also lets us save and load the
 * last active project index.
 */

// ROUTES FOR PARTIALS

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('sign-in', {
      url: "/sign-in",
      templateUrl: "partials/sign-in.html",
      controller: 'SignInCtrl'
    })
    .state('sign-up', {
      url: "/sign-up",
      templateUrl: "partials/sign-up.html",
      controller: 'SignUpCtrl'
    })
     .state('add-business', {
      url: "/add-business",
      templateUrl: "partials/add-business.html",
      controller: 'AddBusinessCtrl'
    })
     .state('find-business', {
      url: "/find-business",
      templateUrl: "partials/find-business.html",
      controller: 'FindBusinessCtrl'
    })
    .state('home', {
      url: "/home",
      templateUrl: "partials/home.html",
      controller: 'HomeCtrl'
    })
    .state('diary', {
      url: "/diary",
      templateUrl: "partials/diary.html",
      controller: 'DiaryCtrl'
    })
    .state('benchmark-neighborhood', {
      url: "/benchmark-neighborhood",
      templateUrl: "partials/benchmark-neighborhood.html",
      controller: 'BenchmarkNeighborhoodCtrl'
    })
    .state('benchmark-vertical', {
      url: "/benchmark-vertical",
      templateUrl: "partials/benchmark-vertical.html",
      controller: 'BenchmarkVerticalCtrl'
    })
    .state('survey-admin', {
      url: "/survey-admin",
      templateUrl: "partials/survey-admin.html",
      controller: 'SurveyAdminCtrl'
    })
    // if none of the above are matched, go to this one
    $urlRouterProvider.otherwise("/sign-in");
})

//CONTROLLERS

.controller('ViewCtrl', function($scope) {
  $scope.views = [
    { title: 'Home' },
    { title: 'Diary' },
    { title: 'Neighborhood Results' },
    { title: 'Industry Results' }
  ];
})

.controller('AddBusinessCtrl', function($scope, $http, $location) {
  console.log('AddBusinessCtrl');
  $scope.addBusiness = function(user) {
    addBusinessUrl = "http://localhost:3000/api/v1/businesses/create_business"
    $http.post(addBusinessUrl, {user:user}, {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      console.log(response);
      $location.path('/home');   
    }).
    error(function(response) {
      console.log(response);
    });
  }
})

.controller('FindBusinessCtrl', function($scope, $http, $location) {
  console.log('FindBusinessCtrl');
  $scope.searchBusiness = function(business) {
    params = 'user[business_name]=' + business.name + '&user[business_zipcode]=' + business.zipcode
    findUrl = 'http://localhost:3000/api/v1/businesses/find_business?' + params
    $http.get(findUrl, {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $scope.results = response.results
      }).
      error(function(response) {
        //need to write what the fuck happens
      });
  };
  $scope.claimBusiness = function(result) {
    addBusinessUrl = "http://localhost:3000/api/v1/businesses/create_business"
    $http.post(addBusinessUrl, {user: {business_name: result.name, business_address: result.formatted_address, business_verticals: result.types}}, {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $location.path('/home');   
    }).
    error(function(response) {
    });
  };    
})

.controller('SignInCtrl', function($scope, $http, $location) {
  console.log('SignInCtrl');
  $scope.signInUser = function(user) {
    $http.post('http://localhost:3000/api/v1/users/sign_in', {user:user}).
      success(function(response) {
        console.log('login successful');
        window.localStorage.setItem("auth_token", response.auth_token);
        $location.path('/home');
      }).
      error(function(response) {
        $scope.errors = response.message;
        console.log('error');
    });
  };
})

.controller('SignUpCtrl', function($scope, $http, $location) {
  console.log('SignUpCtrl');
  $scope.registerUser = function(user) {
    $http.post('http://localhost:3000/api/v1/users.json', {user:user}).
      success(function(response) {
        console.log('login successful');
        window.localStorage.setItem("auth_token", response.auth_token);
        $location.path('/find-business');
      }).
      error(function(response) {
        console.log('error');
    });
  };
})

.controller('HomeCtrl', function($scope, $http, $rootScope, $location) {
  console.log('HomeCtrl');

  //GET active questions to respond to 
  $http.get('http://localhost:3000/api/v1/surveys', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      console.log('surveys got!');
      $scope.questions = response
      $scope.options = response.options
    }).
    error(function(response) {
      debugger
    });

  //Submit User's responses from the survey 
  $scope.submitResponse = function(question, option) {
    $http.post('http://localhost:3000/api/v1/surveys', {option: {text: option, question_id: question.question_id}}, {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      console.log('responsesubmitted');
    }).
    error(function(response) {
      debugger
    });
  }

  //POST diary entry 
  $scope.createEntry = function(entry) {
    $http.post('http://localhost:3000/api/v1/conversations', {conversation: {content: entry.title}}, {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      console.log('responsesubmitted');
      $location.path('/diary');
    }).
    error(function(response) {
      debugger
    });
  };

})


.controller('BenchmarkNeighborhoodCtrl', function($scope, $rootScope, $http) {
  console.log('BenchmarkNeighborhoodCtrl');


  //Get overall results for the survey 
  $http.get('http://localhost:3000/api/v1/surveys/results', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $scope.individual_results = response.individual_results
      $scope.vertical_results = response.vertical_results
      $scope.neighborhood_results = response.neighborhood_results
      $scope.answer_key = response.answer_key
      $rootScope.question_key =   response.question_key
      console.log('results got!');

  //REARRANGE NEIGHBORHOOD DATA FOR POPULATION OF CHARTS
      $rootScope.neighborhood_chart_data = {} 
      for(var question in $scope.neighborhood_results) {
        $rootScope.neighborhood_chart_data[question] = new Array()
        for(var date in $scope.neighborhood_results[question]) {
          tempArray = new Array()
          tempArray.push(Date.parse(date));
          for(var option_id in $scope.neighborhood_results[question][date]) {
            tempArray.push($scope.neighborhood_results[question][date][option_id])
          }

          if(typeof tempArray[2] === 'undefined') {
            numerator = 0
          }
          else {
            numerator = tempArray[2]
         }
          
          percentage = numerator/(tempArray[1]+numerator)
          $rootScope.neighborhood_chart_data[question].push([tempArray[0], percentage])
        }

        $rootScope.neighborhood_chart_data[question].sort(function(a,b) {
          return parseInt(a[0],10) - parseInt(b[0],10);
        })
      };
    }).
      error(function(response) {
       debugger
    });

})

.controller('BenchmarkVerticalCtrl', function($scope, $rootScope, $http) {
  console.log('BenchmarkVerticalCtrl');


  //Get overall results for the survey 
  $http.get('http://localhost:3000/api/v1/surveys/results', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $scope.individual_results = response.individual_results
      $scope.vertical_results = response.vertical_results
      $scope.neighborhood_results = response.neighborhood_results
      $scope.answer_key = response.answer_key
      $rootScope.question_key =   response.question_key
      console.log('results got!');

  //REARRANGE VERTICAL DATA FOR POPULATION OF CHARTS
      $rootScope.vertical_chart_data = {} 
      for(var question in $scope.vertical_results) {
        $rootScope.vertical_chart_data[question] = new Array()
        for(var date in $scope.vertical_results[question]) {
          tempArray = new Array()
          tempArray.push(Date.parse(date));
          for(var option_id in $scope.vertical_results[question][date]) {
            tempArray.push($scope.vertical_results[question][date][option_id])
          }

          if(typeof tempArray[2] === 'undefined') {
            numerator = 0
          }
          else {
            numerator = tempArray[2]
         }
          
          percentage = numerator/(tempArray[1]+numerator)
          $rootScope.vertical_chart_data[question].push([tempArray[0], percentage])
        }

        $rootScope.vertical_chart_data[question].sort(function(a,b) {
          return parseInt(a[0],10) - parseInt(b[0],10);
        })
      };
   }); 
})

.controller('DiaryCtrl', function($scope, $http, $rootScope) {
  console.log('DiaryCtrl');
  
  //Get overall results for the survey 
  $http.get('http://localhost:3000/api/v1/surveys/results', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $scope.individual_results = response.individual_results
      $scope.vertical_results = response.vertical_results
      $scope.neighborhood_results = response.neighborhood_results
      $scope.answer_key = response.answer_key
      $rootScope.question_key =   response.question_key
      console.log('results got!');


  //Get overall results for the survey 
  $http.get('http://localhost:3000/api/v1/conversations', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      $scope.conversation = response
      console.log('results got!')
      debugger
    }).
  error(function(response) {
      debugger
  });
});    

  
  
  //$http.get('http://localhost:3000/api/v1/surveys/results', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
  //  success(function(response) {
  //    $scope.individual_results = response.individual_results
  //    $scope.vertical_results = response.vertical_results
  //    $scope.neighborhood_results = response.neighborhood_results
  //    $scope.answer_key = response.answer_key
  //    $rootScope.question_key =   response.question_key
  //    console.log('results got!');
  //
  //    var ordered_results = [];  
  //    for (i=0; i< $scope.individual_results['How has business been this week?'].length; i++) {
  //        ordered_results.push($scope.individual_results['How has business been this week?'][i]);
  //      }
  //    debugger
  //    for (i=0; i< $scope.individual_results['How much time did you spend doing paperwork today?'].length; i++) {
  //        ordered_results.push($scope.individual_results['How much time did you spend doing paperwork today?'][i]);
  //    }
  //    $scope.ordered_results = ordered_results
  //    $scope.orderProp = 'option_id'
  //    debugger
  //  }).
  //  error(function(response) {
  //    debugger
  // });

    
})

.controller('VerticalChartController', function ($scope, $rootScope) {
    $scope.config = {
          options: {
              chart: {
                  type: 'line'
              },
              legend: {
                   enabled: false
              },
          },
          series: [{"name": $rootScope.question_key[$scope.question], "data": $rootScope.vertical_chart_data[$scope.question],  'lineWidth': 3, "color": '#FFE573'}],
          title: {
              text: $rootScope.question_key[$scope.question]
          },
          xAxis: {
              type: 'datetime'
          },
          
          credits: {
              enabled: false
          },
          loading: false
    }
})

.controller('NeighborhoodChartController', function ($scope, $rootScope) {
    $scope.config = {
          options: {
              chart: {
                  type: 'line'
              },
              legend: {
                  enabled: false
              },
          },
          series: [{"name": $rootScope.question_key[$scope.question], "data": $rootScope.neighborhood_chart_data[$scope.question],  'lineWidth': 3, "color": '#60D5AC'}],
          title: {
              text: $rootScope.question_key[$scope.question]
          },
          
          xAxis: {
              type: 'datetime'
               },
          credits: {
              enabled: false
          },
          loading: false
    }
})

.controller('SurveyAdminCtrl', function($scope, $http) {
  console.log('SurveyAdminCtrl');

  $http.get('http://localhost:3000/api/v1/surveys', {headers: {'user-token': window.localStorage.getItem("auth_token")}}).
    success(function(response) {
      console.log('surveys got!');
      $scope.questions = response
    }).
    error(function(response) {
      //NEED TO HANDLE ERRORS
    });

  $scope.createSurvey = function(survey) {
    $http.post('http://localhost:3000/api/v1/surveys/admin_survey', {survey:survey}).
      success(function(response) {
        console.log('survey created');
        $scope.questions.push({question_text: survey.question_text});
        $scope.questions[$scope.questions.length-1]["options"] = [survey.question_option1, survey.question_option2]
        debugger
        // $scope.question-form.
      }).
      error(function(response) {
        //NEED TO HANDLE ERRORS
      }); 
  }
  
  // $scope.createQuestion = function(question) {
  //   $http.post('http://localhost:3000/api/v1/surveys/admin_question', {question:question}).
  //     success(function(response) {
  //       console.log('question created');
  //     }).
  //     error(function(response) {
  //       debugger
  //     }); 
  // }

})

.controller('TodoCtrl', function($scope, $timeout, $ionicModal) {
  console.log('TodoCtrl');
  //NEED TO EVENTUALLY DELETE THIS 
});

